import React from "react";
import Router from "next/router";

import LoginIcon from "@mui/icons-material/Login";
import HourglassEmptyIcon from "@mui/icons-material/HourglassEmpty";
import { Alert, IconButton } from "@mui/material";
import { useUser } from "@auth0/nextjs-auth0/client";
import BasicMenu from "./BasicMenu";

const AuthButton = () => {
  const { user, error, isLoading } = useUser();

  if (isLoading)
    return (
      <div>
        <HourglassEmptyIcon></HourglassEmptyIcon>
      </div>
    );
  if (error) {
    console.log(error);
    return <div>{error.message}</div>;
  }

  return (
    <>
      {!user && !isLoading && (
        <IconButton
          color="inherit"
          onClick={() => Router.replace("/api/auth/login")}
        >
          <LoginIcon color="inherit"></LoginIcon>
        </IconButton>
      )}
      {user && !isLoading && (
        <BasicMenu
          menuitems={[
            { name: "Profile", link: "/profile" },
            { name: "Logout", link: "/api/auth/logout" },
          ]}
        ></BasicMenu>
      )}
      {error && (
        <Alert severity="error" onClose={() => {}}>
          There was an error from the authentication provider. Check console for
          details.
        </Alert>
      )}
    </>
  );
};

export default AuthButton;
