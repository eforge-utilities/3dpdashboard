import mongoose, { Schema, model, Types } from "mongoose";

export interface IUser {
  _id: Types.ObjectId;
  email: string;
  interfaceTheme: "light" | "dark";
}

export const userSchema = new Schema<IUser>({
  email: { type: String, required: true },
  interfaceTheme: { type: String, required: true },
});

export const Users = mongoose.models.Users || model<IUser>("Users", userSchema);
