import React from "react";

export const ThemeContext = React.createContext({
  theme: "",
  isLoading: false,
  isValidating: false,
  themeChanged: () => {},
});
