// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import dbConnect from "../../../db/dbConnect";
import { IUser, Users } from "../../../db/User";
import { getSession, withApiAuthRequired } from "@auth0/nextjs-auth0";

type Data = {
  name: string;
};

export default withApiAuthRequired(async function handler(
  req: NextApiRequest,
  res: NextApiResponse<IUser>
) {
  if (req.method === "GET") {
    const session = await getSession(req, res);

    if (session) {
      await dbConnect();
      const user = await Users.findOne({ email: session.user.email });
      if (user) {
        res.status(200).send(user);
      } else {
        //else lets create it
        const newUser = await Users.create({
          email: session.user.email,
          interfaceTheme: "dark",
        });
        const created = await newUser.save();
        res.status(200).end(created);
      }
    }
  } else if (req.method === "PUT") {
    const theme = JSON.parse(req.body).interfaceTheme;
    if (theme !== "light" && theme !== "dark") {
      console.log("invalid interface theme" + theme);
      res.status(400).end();
      return;
    }
    const session = await getSession(req, res);

    if (session) {
      await dbConnect();
      const ub = await Users.findOne({ email: session.user.email });
      if (ub) {
        ub.interfaceTheme = theme;
        const updated = await ub.save();
        res.status(200).send(updated);
      } else {
        res.status(500).end();
      }
    }
  } else {
    res.status(405).end();
  }
});
