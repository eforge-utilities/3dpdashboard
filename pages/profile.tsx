import useSWR from "swr";
import { withPageAuthRequired } from "@auth0/nextjs-auth0";
import {
  Alert,
  AlertTitle,
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Snackbar,
  Typography,
} from "@mui/material";
import Head from "next/head";
import { useContext, useState } from "react";
import { ThemeContext } from "@/lib/contexts/theme-context";

const ProfilePage = () => {
  const { data, error, isLoading, mutate } = useSWR("/api/user/me");
  const [showAlert, setShowAlert] = useState(false);
  const themeContext = useContext(ThemeContext);
  const handleThemeChange = async (newThemeValue: "light" | "dark") => {
    const response = await fetch("/api/user/me", {
      method: "PUT",
      body: JSON.stringify({ interfaceTheme: newThemeValue }),
    });
    if (response && response.ok) {
      //setSuccessMessage("Folder moved successfully.");
      //setShowRemoveFolder(false);
      //reloadData();
      mutate();
      themeContext.themeChanged();
      setShowAlert(true);
      setTimeout(() => setShowAlert(false), 3000);
    } else {
    }
  };

  return (
    <>
      <Head>
        <title>3DP Dashboard - Profile</title>
      </Head>
      <div>
        {showAlert && (
          <Snackbar
            open={showAlert}
            autoHideDuration={6000}
            onClose={() => setShowAlert(false)}
          >
            <Alert
              onClose={() => setShowAlert(false)}
              severity="success"
              sx={{ width: "100%" }}
            >
              Save was successful.
            </Alert>
          </Snackbar>
        )}
        <Typography variant="h3">Profile Page</Typography>
        {!isLoading && (
          <Box sx={{ minWidth: 120 }}>
            <FormControl fullWidth>
              <InputLabel id="demo-simple-select-label">
                Display Mode
              </InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                label="Display Mode"
                value={data.interfaceTheme}
                onChange={(e) => {
                  handleThemeChange(e.target.value);
                }}
              >
                <MenuItem value="light">Light</MenuItem>
                <MenuItem value="dark">Dark</MenuItem>
              </Select>
            </FormControl>
          </Box>
        )}
      </div>
    </>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default ProfilePage;
