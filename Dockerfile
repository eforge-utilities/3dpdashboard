# Dockerfile
#####Common architechture here
# base image
FROM --platform=$BUILDPLATFORM node:latest AS build

# create & set working directory
WORKDIR /app
COPY package*.json ./

# install dependencies
RUN npm install

# copy source files
COPY . .

# start app
RUN npm run build

#####here pull the specific for both Arch
FROM node:latest
WORKDIR /app
COPY --from=build /app .
EXPOSE 3000
CMD ["npm", "start"]
